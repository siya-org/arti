# Generated by Django 3.0.4 on 2020-03-13 07:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Paragraph',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('font_size', models.IntegerField()),
                ('paragraph_no', models.IntegerField(default=1)),
                ('justification', models.CharField(choices=[('C', 'Center'), ('L', 'Left'), ('R', 'Right')], default='C', max_length=6)),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pages.Page')),
            ],
        ),
    ]
