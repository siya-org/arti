"""arti URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from book.views import BookList, BookDetailView, BookCreateView, BookUpdateView, BookDeleteView, BookUpdateStateView, request_verification, deny_verification, verify, cancel_verification, BookSearch, AllBooksBeingTranslated, AllBooksDeniedTranslation, AllBooksRequestingVF, AllBooksVerified, UserBooksBeingTranslated, UserBooksDeniedTranslation, UserBooksRequestingVF,UserBooksVerified
from paragraph.views import ParagraphList, ParagraphForPageCreateView, ParagraphForPageUpdateView, ParagraphDeleteView
from pages.views import PageList, PageDetailView, PageForBookCreateView, PageForBookUpdateView, PageDeleteView
from django.conf.urls.static import static
from django.conf import settings
from PDF.views import generate_pdf
from . import views as core_views
from accounts.views import make_admin, UserList
urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signup/', core_views.signup, name='signup'),
    path('', BookList.as_view(), name='book-list' ),
    path('paragraph/', ParagraphList.as_view(), name='paragraph-list'),
    path('page/', PageList.as_view(), name='page-list'),
    path('book/<pk>', BookDetailView.as_view(), name='book-detail'),
    path('page/<pk>', PageDetailView.as_view(), name='page-detail'),
    path('create-book/', BookCreateView.as_view(), name='book-create'),
    path('create-page/<book_id>/', PageForBookCreateView.as_view(), name='page-create'),
    path('create-paragraph/<page_id>/', ParagraphForPageCreateView.as_view(), name='paragraph-create'),
    path('update-paragraph/<pk>', ParagraphForPageUpdateView.as_view(), name='paragraph-update'),
    path('update-page/<pk>', PageForBookUpdateView.as_view(), name='page-update'),
    path('update-book/<pk>', BookUpdateView.as_view(), name='book-update'),
    path('delete-book/<pk>', BookDeleteView.as_view(), name='book-delete'),
    path('delete-page/<pk>', PageDeleteView.as_view(), name='page-delete'),
    path('delete-paragraph/<pk>', ParagraphDeleteView.as_view(), name='paragraph-delete'),
    path('generate-pdf/<book_id>', generate_pdf, name='generate-pdf'),
    path('make-admin/<user_id>', make_admin, name='make-admin'),
    path('user-list', UserList.as_view(), name='user-list'),
    path('edit-state/<pk>', BookUpdateStateView.as_view(), name='edit-state'),
    path('request-verification/<book_id>', request_verification, name='request-verification'),
    path('deny-verification/<book_id>', deny_verification, name='deny-verification'),
    path('verify/<book_id>', verify, name='verify'),
    path('cancel-verification/<book_id>', cancel_verification, name='cancel-verification'),
    path('book-search', BookSearch.as_view(), name='book-search'),
    path('book-being-translated', AllBooksBeingTranslated.as_view(), name='book-being-translated'),
    path('book-verified', AllBooksVerified.as_view(), name='book-verified'),
    path('book-denied-translation', AllBooksDeniedTranslation.as_view(), name='book-denied-translation'),
    path('book-request-verification', AllBooksRequestingVF.as_view(), name='book-request-verification'),
    path('book-request-verification', AllBooksRequestingVF.as_view(), name='book-request-verification'),
    path('user-book-verification', UserBooksRequestingVF.as_view(), name='user-book-verification'),
    path('user-book-translated', UserBooksBeingTranslated.as_view(), name='user-book-translated'),
    path('user-book-denied', UserBooksDeniedTranslation.as_view(), name='user-book-denied'),
    path('user-book-verified', UserBooksVerified.as_view(), name='user-book-verified'),
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

