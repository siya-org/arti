# Generated by Django 3.0.4 on 2020-03-19 06:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0009_auto_20200319_0647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='book',
            name='price',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='book',
            name='publisher_name',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='book',
            name='publisher_place',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='book',
            name='series',
            field=models.TextField(default=''),
        ),
    ]
